-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 04:12 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scg_indonesia`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kategori` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qr_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`id`, `nama`, `kategori`, `qr_code`) VALUES
('161735740189431', 'pc inkejt', 'Sistem Komputer', '1617357401894316.png');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `id` int(11) NOT NULL,
  `who_borrowed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_borrowed` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_item`, `description_category`) VALUES
(1, 'Personal Computer', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(2, 'Printer', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(3, 'Uninterruptible Power Supply', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(4, 'Software', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(5, 'Router', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(6, 'Switch', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ');

-- --------------------------------------------------------

--
-- Table structure for table `damage`
--

CREATE TABLE `damage` (
  `id` int(11) NOT NULL,
  `date_damage` date NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_estimated` float NOT NULL,
  `date_repaired` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_item` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `building` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_purchase` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_damage` int(11) NOT NULL,
  `id_borrow` int(11) NOT NULL,
  `id_return` int(11) NOT NULL,
  `qr_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name_item`, `room`, `building`, `location`, `id_purchase`, `id_status`, `id_category`, `id_damage`, `id_borrow`, `id_return`, `qr_code`) VALUES
('FA00000007', 'UPS X123', 'srmi', 'graha mobisel', 'Head Office', 'FA00000007PC', 2, 3, 0, 0, 0, 'FA00000007.png'),
('FA00000009', 'Test', 'srmi', 'graha mobisel', 'Head Office', 'FA00000009PC', 6, 6, 0, 0, 0, 'FA00000009.png'),
('FA0000008', 'Notebook123', 'srmi', 'graha mobisel', 'Head Office', 'FA0000008PC', 1, 1, 0, 0, 0, 'FA0000008.png'),
('FA000001', 'Hp desktop c360', 'SRMI', 'graha mobisel', 'Head Office', 'FA000001PC', 1, 1, 0, 0, 0, 'FA000001.png'),
('FA000002', 'PC INKJET 2000', 'Pipe & Prec', 'mobisel', 'Head Office', 'FA000002PC', 2, 1, 0, 0, 0, 'FA000002.png'),
('FA000003', 'Cisco Router x70', 'SRMI', 'Graha Mobisel', 'Head Office', 'FA000003PC', 1, 5, 0, 0, 0, 'FA000003.png'),
('FA000004', 'CD Windows 10 Home', 'SRMI', 'Graha Mobisel', 'Head Office', 'FA000004PC', 3, 4, 0, 0, 0, 'FA000004.png');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `who_purchase` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_purchase` date NOT NULL,
  `price` float NOT NULL,
  `who_received` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_received` date NOT NULL,
  `range_shrinked` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`id`, `who_purchase`, `date_purchase`, `price`, `who_received`, `date_received`, `range_shrinked`) VALUES
('FA00000007PC', 'dadang', '2019-08-19', 3500000, 'yusuf', '2019-08-19', 6),
('FA00000009PC', 'bebi', '2019-08-23', 566666, 'bayu', '2019-08-24', 5),
('FA0000008PC', 'dadang', '2019-08-19', 8000000, 'kiki', '2019-08-20', 5),
('FA000001PC', 'dadang', '2019-07-26', 4500000, 'bayu', '2019-07-27', 4),
('FA000002PC', 'dadang', '2019-08-05', 450000, 'yusuf', '2019-08-15', 4),
('FA000003PC', 'dadang', '2019-08-13', 15000000, 'Bayu', '2019-08-14', 3),
('FA000004PC', 'dadang', '2019-08-19', 350000, 'Bayu', '2019-08-21', 5);

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE `return` (
  `id` int(11) NOT NULL,
  `who_returned` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_returned` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_item`, `description_status`) VALUES
(1, 'New', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(2, 'Use', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(3, 'Spare', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(4, 'Broken', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(5, 'Service', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. '),
(6, 'Scrap', 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jenis_user` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `id_jenis_user`, `image`) VALUES
(5, 'venna', 'venna123', 3, 'default.jpg'),
(29, 'sachi', 'sachi', 1, 'sachi.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `damage`
--
ALTER TABLE `damage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_purchase` (`id_purchase`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `damage`
--
ALTER TABLE `damage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return`
--
ALTER TABLE `return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`id_purchase`) REFERENCES `purchase` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
