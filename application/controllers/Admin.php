<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('item_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        if($this->session->userdata('id_jenis_user') <> '1')
        {
            redirect('User/login');
        }
    }

    public function index(){
        $d['user_session'] = $this->session->userdata('username');
        $d['image_session'] = $this->session->userdata('image');
        $d['total_invest']= $this->item_model->get_total_invest();
        $d['total_item'] = $this->item_model->get_total_item();
        $d['total_status'] = $this->item_model->get_total_status();
        $d['total_category'] = $this->item_model->get_total_category();
        $d['pie_data'] = $this->item_model->pie_chart();
        $d['bar_data'] = $this->item_model->bar_chart();
        $d['pie_data2'] = $this->auth_model->pie_chart2();
        $d['polar_data'] = $this->item_model->polar_chart();

        $d['get_status'] = $this->item_model->get_status();
        $d['get_room'] = $this->item_model->get_room();
        $this->load->view('dashboard_view', $d);
//        echo("<script>console.log('PHP: " . $d['total_invest'][] . "');</script>");
    }

    //User controller
    private function upload_image($username){
        $config['upload_path']          = './assets/images/user';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $username;
        $config['overwrite']			= true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('image')){
            return $this->upload->data("file_name");
        }

        print_r($this->upload->display_errors());

        return "default.jpg";
    }

    function save_user() {
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $id_jenis_user=$this->input->post('id_jenis_user');
        $image=$this->upload_image($username);

        $this->auth_model->add_user($username, $password, $id_jenis_user, $image);
        redirect('admin/user');
    }

    function user() {
        $d['user_session'] = $this->session->userdata('username');
        $d['data'] = $this->auth_model->get_user();
        $this->load->view('admin/user_view', $d);
    }

    function update_user($id){
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $id_jenis_user=$this->input->post('id_jenis_user');

        if (!empty($_FILES["image"]["name"])) {
            $image = $this->upload_image($username);
//            print_r('yeyy, berhasil ganti');
        } else {
            $image = $this->input->post('old_image');
//            print_r('hello');
        }

        $this->auth_model->renew_user($id, $username, $password, $id_jenis_user, $image);
        redirect('admin/user');
    }

    function delete_user($id) {
        $this->auth_model->remove_user($id);
        redirect('admin/user');
    }


    //Item Controller
    function item() {
        $d['user_session'] = $this->session->userdata('username');
        $d['data']=$this->item_model->get_all_item();
        $this->load->view('admin/item_view', $d);
    }

    function input() {
      //$s['user_session'] = $this->session->userdata('username');    //tidak ngebaca ketika diparsingkarna ada array dibawah
        $s = array(
            'ss_status' => $this->item_model->get_status(),
            'dd_category' => $this->item_model->get_category()
        );
        $this->load->view('admin/input_view', $s);
    }

    function save_item(){
        $id=$this->input->post('id');
        $name_item=$this->input->post('name_item');
        $id_status=$this->input->post('id_status');
        $id_category=$this->input->post('id_category');
        $room=$this->input->post('room');
        $building=$this->input->post('building');
        $location=$this->input->post('location');

        $id_purchase=($id.'PC');
        $id_item=$this->input->post('id');
        if($id_category == '1'){
            $id_special=($id.'SP');
        }else{
            $id_special=('0'.$id.'SP');
        }
        $who_purchase=$this->input->post('who_purchase');
        $date_purchase=$this->input->post('date_purchase');
        $price=$this->input->post('price');
        $who_received=$this->input->post('who_received');
        $date_received=$this->input->post('date_received');
        $range_shrinked=$this->input->post('range_shrinked');

        $brand=$this->input->post('brand');
        $spec_1=$this->input->post('spec_1');
        $spec_2=$this->input->post('spec_2');

        $this->load->library('ciqrcode');

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name=$id.'.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = ('192.168.8.101/SCG/IT_Asset/user/search?param1='.$id); //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 20;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $this->item_model->add_item($id,$name_item,$room,$building,$location,$id_status,$id_category, $image_name, $id_purchase, $id_item, $id_special, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked, $brand, $spec_1, $spec_2); //simpan ke database
        redirect('admin/item'); //redirect ke asset usai simpan data
    }

    function edit_item($id){
        $data['default'] = $this->item_model->get_default($id);
        $data['data_status']=$this->item_model->get_status();
        $data['data_category']=$this->item_model->get_category();
        $this->load->view("admin/edit_view",$data);
    }

    function update_item($id){
//        $id=$this->input->post('id');
        $id_parsing = $id;
        $name_item=$this->input->post('name_item');
        $id_status=$this->input->post('id_status');
        $id_category=$this->input->post('id_category');
        $room=$this->input->post('room');
        $building=$this->input->post('building');
        $location=$this->input->post('location');

        if ($id_status=="0") {
            $id_status = $this->input->post('old_id_status');
        } else {
            $id_status=$this->input->post('id_status');
        }

        if ($id_category=="0") {
            $id_category = $this->input->post('old_id_category');
        } else {
            $id_category=$this->input->post('id_category');
        }

//        $id_item=$this->input->post('id');
        $who_purchase=$this->input->post('who_purchase');
        $date_purchase=$this->input->post('date_purchase');
        $price=$this->input->post('price');
        $who_received=$this->input->post('who_received');
        $date_received=$this->input->post('date_received');
        $range_shrinked=$this->input->post('range_shrinked');

        $brand=$this->input->post('brand');
        $spec_1=$this->input->post('spec_1');
        $spec_2=$this->input->post('spec_2');

        $id_special = $this->input->post('id_special');
        $processor=$this->input->post('processor');
        $ram=$this->input->post('ram');
        $disk=$this->input->post('disk');
        $os=$this->input->post('os');
        $screen=$this->input->post('screen');
        $port=$this->input->post('port');

        $this->item_model->renew_item($id_parsing,$name_item,$room,$building,$location,$id_status,$id_category,$id_special,$who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked, $brand, $spec_1, $spec_2, $processor, $ram, $disk, $os, $screen, $port); //simpan ke database
        redirect('admin/item'); //redirect ke asset usai simpan data
    }

    function delete_item($id){
        $this->item_model->remove_item($id);
        redirect('admin/item');
    }

    function print_qrcode($id){
        $this->load->library('pdf');
        $data['id']=$id;
        $data['data'] = $this->item_model->get_search_item($id);
    //        echo("<script>console.log('PHP: " . $data. "');</script>");

        $this->load->library('pdf');
        $this->pdf->setPaper('A6', 'landscape');
        $this->pdf->filename = "laporan-item.pdf";
        //untuk parsing image qrcode
        $this->pdf->set_option('isRemoteEnabled',true);
        $this->pdf->load_view('admin/qrcode_view', $data);
    }

    function print_item(){

        $this->load->library("excel");

        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);

        $table_columns = array("ID Item", "Item Name", "Room", "Building", "Status", "Category");

        $column = 0;

        foreach($table_columns as $field){

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

            $column++;

        }

        $employee_data = $this->item_model->get_all_item();

        $excel_row = 2;

        foreach($employee_data as $row){

            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->name_item);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->room);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->building);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->status_item);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->category_item);

            $excel_row++;

        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

        header('Content-Type: application/vnd.ms-excel');

        header('Content-Disposition: attachment;filename="All-SCG-Asset-Data.xls"');

        $object_writer->save('php://output');
    }

    //Status_Item controller
    function save_status() {
        $status_item = $this->input->post('status_item');
        $description_status = $this->input->post('description_status');

        $this->item_model->add_status($status_item, $description_status);
        redirect('admin/status');
    }

    function status() {
        $d['user_session'] = $this->session->userdata('username');
        $d['data'] = $this->item_model->get_status();
        $this->load->view('admin/status_view', $d);
    }

    function update_status($id){
        $status_item=$this->input->post('edit_status');
        $description_status=$this->input->post('edit_description');
        $this->item_model->renew_status($id, $status_item, $description_status);
        redirect('admin/status');
    }

    function delete_status($id) {
        $this->item_model->remove_status($id);
        redirect('admin/status');
    }

    //Category_Item controller
    function save_category() {
        $category_item = $this->input->post('category_item');
        $description_category = $this->input->post('description_category');

        $this->item_model->add_category($category_item, $description_category);
        redirect('admin/category');
    }

    function category() {
        $d['user_session'] = $this->session->userdata('username');
        $d['data'] = $this->item_model->get_category();
        $this->load->view('admin/category_view', $d);
    }

    function update_category($id){
        $category_item=$this->input->post('edit_category');
        $description_category=$this->input->post('edit_description');
        $this->item_model->renew_category($id, $category_item, $description_category);
        redirect('admin/category');
    }

    function delete_category($id){
        $this->item_model->remove_category($id);
        redirect('admin/category');
    }

    function damage() {
        $d['user_session'] = $this->session->userdata('username');
        $this->load->view('admin/damage_view', $d);
    }

    function borrow() {
        $d['user_session'] = $this->session->userdata('username');
        $this->load->view('admin/borrow_view', $d);
    }

    function returnitem() {
        $d['user_session'] = $this->session->userdata('username');
        $this->load->view('admin/return_view', $d);
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('user/login');
    }
}