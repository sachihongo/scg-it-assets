<?php
class Test extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('test_model','item');
	}

	function index(){
        $d['data']=$this->item->get_all_item();
        $d['ss_status']=$this->item->get_status();
        $d['dd_category']=$this->item->get_category();
        $this->load->view("test_view", $d);
	}

    function save_item(){
        $id=$this->input->post('id');
        $name_item=$this->input->post('name_item');
        $id_status=$this->input->post('id_status');
        $id_category=$this->input->post('id_category');
        $room=$this->input->post('room');
        $building=$this->input->post('building');

        $id_purchase=($id.'PC');
        $id_item=$this->input->post('id');
        $who_purchase=$this->input->post('who_purchase');
        $date_purchase=$this->input->post('date_purchase');
        $price=$this->input->post('price');
        $who_received=$this->input->post('who_received');
        $date_received=$this->input->post('date_received');
        $range_shrinked=$this->input->post('range_shrinked');

        $this->load->library('ciqrcode');

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name=$id.'.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = ('192.168.8.100/SCG/IT_Asset/user/search?param1='.$id); //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 20;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $this->item->add_item($id,$name_item,$room,$building,$id_status,$id_category, $image_name, $id_purchase, $id_item, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked); //simpan ke database
        redirect('test'); //redirect ke asset usai simpan data
    }

}
