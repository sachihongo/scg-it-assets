<?php
class Asset extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('asset_model');
	}

	function index(){
		$data['data'] = $this->asset_model->get_all_asset();
		$this->load->view('asset_view', $data);
	}

	function simpan(){
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		$kategori=$this->input->post('kategori');

		$this->load->library('ciqrcode');

		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name=$id.'.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = ('www.scgindonesia.com/item/'.$id); //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 20;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $this->asset_model->simpan_asset($id,$nama,$kategori,$image_name); //simpan ke database
        redirect('asset'); //redirect ke asset usai simpan data
	}
}
