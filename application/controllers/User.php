<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('item_model');
//        if($this->session->userdata('id_jenis_user') <> '1')
//        {
//            redirect('mahasiswa');
//        }
    }

    function login_asset($id) {
        $d['identitas'] = $id;
//        echo("<script>console.log('PHP: ".$d."');</script>"); //for debugiing console.log
        $this->load->view('login_view_asset', $d);
    }

    function authloginasset($id) {
        $this->load->model('auth_model');
        $this->load->library('session');
        $this->data['error']='';
        $username = $this->input->post('form-username');
        $password = $this->input->post('form-password');

        $cek = $this->auth_model->cek($username, $password);
        if($cek->num_rows() == 1) {
            foreach ($cek->result() as $data) {
                $sess_data['id_user'] = $data->id_user;
                $sess_data['username'] = $data->username;
                $sess_data['id_jenis_user'] = $data->id_jenis_user;
                $this->session->set_userdata($sess_data);
            }

            if ($this->session->userdata('id_jenis_user') == '3') {
                $d['data'] = $this->item_model->get_search_item_2($id);
                $this->load->view('item_view', $d);
            }
        }
    }

    function login() {
        $this->load->view('login_view');
    }

    function authlogin() {
        $this->load->model('auth_model');
        $this->load->library('session');
        $this->data['error']='';
        $username = $this->input->post('form-username');
        $password = $this->input->post('form-password');

        $cek = $this->auth_model->cek($username, $password);
        if($cek->num_rows() == 1)
        {
            foreach($cek->result() as $data){
                $sess_data['id_user'] = $data->id_user;
                $sess_data['username'] = $data->username;
                $sess_data['id_jenis_user'] = $data->id_jenis_user;
                $sess_data['image'] = $data->image;
                $this->session->set_userdata($sess_data);
            }

            if($this->session->userdata('id_jenis_user') == '1')
            {
                redirect('Admin');
            }
            elseif($this->session->userdata('id_jenis_user') == '2')
            {
                redirect('Staff');
            }
            elseif($this->session->userdata('id_jenis_user') == '3')
            {
//                redirect('User');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan', 'Maaf, kombinasi username dengan password salah.');
            redirect('User/login');
        }
    }

    function transfer_item($id){
        $room=$this->input->post('edit_room');
        $building=$this->input->post('edit_building');
        $location=$this->input->post('edit_location');
//        echo("<script>console.log('PHP: ".$room."');</script>"); //for debugiing console.log
        $this->item_model->relocate_item($id, $room, $building, $location); //simpan ke database
        $d['data'] = $this->item_model->get_search_item($id);
        $this->load->view('item_view', $d);
    }

    function search(){
//        $d['user_session'] = $this->session->userdata('username');
        //cara manggil di paramp: http://example.com/index.php/controller_name?param1=value&param2=value
//        echo $this->input->get('param1');
        $parsing = $this->input->get('param1');
        $d['data'] = $this->item_model->get_search_item_2($parsing);
        $this->load->view('item_view', $d);
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('');
    }

}