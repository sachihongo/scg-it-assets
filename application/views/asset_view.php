<!DOCTYPE html>
<html>
<head>
	<title>Data Asset</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
<div class="container">
	<div class="row">
		<h2>Data <small>ASSET IT PT.SCG ReadyMix</small></h2>
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add New</button>
		<table class="table table-striped">
			<thead>
			<tr>
				<th>ID</th>
				<th>NAMA</th>
				<th>KATEGORI</th>
				<th>QR CODE</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($data->result() as $row):?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $row->id;?></td>
				<td style="vertical-align: middle;"><?php echo $row->nama;?></td>
				<td style="vertical-align: middle;"><?php echo $row->kategori;?></td>
				<td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row->qr_code;?>"></td>
			</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal add new mahasiswa-->
<form action="<?php echo base_url().'index.php/asset/simpan'?>" method="post">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add New Item</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="id" class="control-label">ID:</label>
						<input type="text" name="id" class="form-control" id="nim">
					</div>
					<div class="form-group">
						<label for="nama" class="control-label">NAMA:</label>
						<input type="text" name="nama" class="form-control" id="nama">
					</div>
					<div class="form-group">
						<label for="prodi" class="control-label">Kategori:</label>
						<select name="kategori" class="form-control" id="prodi">
							<option>Sistem Informasi</option>
							<option>Sistem Komputer</option>
							<option>Manajemen Informatika</option>
						</select>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.4.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
</body>
</html>
