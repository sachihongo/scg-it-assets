<!DOCTYPE html>
<html>
<head>
    <title>Data Asset</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>

        <table class="table">
            <tr>
                <th>PT. SCG Ready Mix</th>
                <td><img style="height: 20px" src="<?php echo base_url().'./assets/images/asset_item.png';?>"</td>
            </tr>
            <tr>
                <th rowspan="4"><img src="<?php echo base_url().'./assets/images/qrcode/'.$data['qr_code'];?>" style="width: 120px"></th>
                <td><b>Equipment ID</b> <?php echo $id;?></td>
            </tr>
            <tr>
                <td><b>Description</b>: <?=isset($data['name_item'])? $data['name_item'] : ""?></td>
            </tr>
            <tr>
                <td><b>Area</b>: <?=isset($data['room'])? $data['room'] : ""?></td>
            </tr>
            <tr>
                <td><b>Date</b>: <?=isset($data['date_purchase'])? $data['date_purchase'] : ""?></td>
            </tr>
            <hr>
        </table>

</body>
</html>
