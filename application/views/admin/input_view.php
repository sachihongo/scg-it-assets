<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url().'assets/vendor/fontawesome-free/css/all.min.css'?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url().'assets/css/sb-admin-2.min.css'?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/logo.png'?>"/>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon rotate-n-0">
                <img src="<?php echo base_url().'assets/images/dsb/scg5.png'?>"></img>
            </div>
            <div class="sidebar-brand-text mx-3">IT Asset</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url("Admin");?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Users -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url("Admin/user");?>">
                <i class="fas fa-fw fa-users"></i>
                <span>User people</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url("Admin/item");?>">
                <i class="fas fa-fw fa-table"></i>
                <span>Table List</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url("Admin/input");?>">
                <i class="fas fa-fw fa-paper-plane"></i>
                <span>Input Item</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Addons
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Detail Item</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Custom Details:</h6>
                    <a class="collapse-item" href="<?php echo base_url("Admin/status");?>">Status</a>
                    <a class="collapse-item" href="<?php echo base_url("Admin/category");?>">Category</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Tables -->
<!--        <li class="nav-item">-->
<!--            <a class="nav-link" href="--><?php //echo base_url("Admin/damage");?><!--">-->
<!--                <i class="fas fa-fw fa-heartbeat"></i>-->
<!--                <span>Damage Item</span></a>-->
<!--        </li>-->

        <!-- Nav Item - Utilities Collapse Menu -->
<!--        <li class="nav-item">-->
<!--            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">-->
<!--                <i class="fas fa-fw fa-wrench"></i>-->
<!--                <span>User Activity</span>-->
<!--            </a>-->
<!--            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">-->
<!--                <div class="bg-white py-2 collapse-inner rounded">-->
<!--                    <h6 class="collapse-header">Activity:</h6>-->
<!--                    <a class="collapse-item" href="--><?php //echo base_url("Admin/borrow");?><!--">Borrow</a>-->
<!--                    <a class="collapse-item" href="--><?php //echo base_url("Admin/return    item");?><!--">Return</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </li>-->

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('username')?></span>
                            <?php if($this->session->userdata('image') == 'default.jpg') : ?>
                                <img class="img-profile rounded-circle" src="<?php echo base_url().'./assets/images/user/default/default.jpg'?>">
                            <?php else : ?>
                                <img class="img-profile rounded-circle" src="<?php echo base_url().'./assets/images/user/'.$this->session->userdata('image');?>">
                            <?php endif; ?>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-1 text-gray-800">Input Item</h1>
                <p class="mb-4">Any asset information or data that is sent to our system for processing is considered input. Input or user input is sent to a our system database by filled form below.</p>

                <!-- Content Row -->
                <form id="myForm" action="<?php echo base_url().'admin/save_item'?>" method="post">
<!--                <form id="ajax_form" method="post" action="javascript:void(0)">-->
                    <div class="row">
                            <!-- Grow In Utility -->
                            <div class="col-lg-6">

                                <div class="card position-relative">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Item</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="text" id="id" name="id" class="form-control" placeholder="Id item" aria-label="Id" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="name_item" name="name_item" class="form-control" placeholder="Item name" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="input-group mb-3">
                                            <select id="id_status" name="id_status" class="custom-select" id="inputGroupSelect01">
                                                <option selected>Choose status...</option>
                                                <?php foreach ($ss_status as $row): ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->status_item; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <select id="id_category" name="id_category" class="custom-select" id="inputGroupSelect02">
                                                <option selected>Choose category...</option>
                                                <?php foreach ($dd_category as $row): ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->category_item; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input id="room" type="text" name="room" class="form-control" placeholder="Room" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input id="building" type="text" name="building" class="form-control" placeholder="Building" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input id="location" type="text" name="location" class="form-control" placeholder="Location" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Fade In Utility -->
                            <div class="col-lg-6">

                                <div class="card position-relative">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Purchase</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input id="who_purchase" type="text" name="who_purchase" class="form-control" placeholder="Who purchase" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon3">Date purchase</span>
                                            </div>
                                            <input id="date" type="date" name="date_purchase" class="form-control" placeholder="Date purhcase" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input id="price" type="number" name="price" class="form-control" placeholder="Price" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input id="who_received" type="text" name="who_received" class="form-control" placeholder="Who received" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon3">Date received</span>
                                            </div>
                                            <input id="date_received" type="date" name="date_received" class="form-control" placeholder="Date received" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                        <div class="form-group">
                                            <input id="range_shrinked" type="number" name="range_shrinked" class="form-control" placeholder="Range shrinked (year)" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>

                            </div>

                    </div>

                    <br>

                    <div class="row">
                        <!-- Grow In Utility -->
                        <div class="col-lg-12">

                            <div class="card position-relative">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Common Spec</h6>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <input type="text" id="brand" name="brand" class="form-control" placeholder="Brand ( You must enter this field )" >
                                    </div>
                                    <div class="form-group">
                                        <textarea type="text" id="spec_1" name="spec_1" class="form-control" placeholder="Com Spec 1 ( If your item is Computer, you may not fill this field and can add spec detail in edit section )" ></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea type="text" id="spec_2" name="spec_2" class="form-control" placeholder="Com Spec 2 ( If your item is Computer, you may not fill this field and can add spec detail in edit section )" ></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12" style="padding-top:10px; padding-bottom: 20px">
                        <button type="submit" id="send_form" class="btn btn-primary btn-block">Insert Item</button>
                    </div>
                </form>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; SCG Indonesia 2019</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url('admin/logout');?>">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url().'assets/vendor/jquery/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/vendor/jquery/jquery.validate.js'?>"></script>
<script>
    $(document).ready(function () {

        $('#myForm').validate({
            rules: {
                id: {
                    // minlength: 2,
                    required: true,
                    maxlength: 10
                },
                name_item: {
                    required: true,
                    maxlength: 50
                },
                room: {
                    required: true,
                },
                building: {
                    required: true
                },
                who_purchase: {
                    required: true,
                },
                date_purchase: {
                    required: true,
                },
                price: {
                    required: true
                },
                who_received: {
                    required: true
                },
                date_received: {
                    required: true
                },
                range_shrinked: {
                    required: true,
                }
            },
            messages: {
                id: {
                    required: "<h6>Please enter Id</h6>",
                    maxlength: "<h6>Your id maxlength should be 10 characters long.</h6>"
                },
                name_item: {
                    required: "<h6>Please enter name</h6>",
                    maxlength: "<h6>Your last name maxlength should be 50 characters long.</h6>"
                },
                room: {
                    required: "<h6>Please enter the room</h6>"
                },
                building: {
                    required: "<h6>Please enter the buiding</h6>"
                },
                who_purchase: {
                    required: "<h6>Please enter the buyer</h6>"
                },
                date_purchase: {
                    required: "<h6>Please enter the date</h6>"
                },
                price: {
                    required: "<h6>Please enter the price</h6>"
                },
                date_received: {
                    required: "<h6>Please enter the date</h6>"
                },
                who_received: {
                    required: "<h6>Please enter the receiver</h6>"
                },
                range_shrinked: {
                    required: "<h6>Please enter the range shrinked</h6>"
                }
            },
            highlight: function (element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
            },
            // success: function (element) {
            //     element.text('OK!').addClass('valid')
            //         .closest('.control-group').removeClass('error').addClass('success');
            // }
        });

    });
</script>

<script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.bundle.min.js'?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url().'assets/vendor/jquery-easing/jquery.easing.min.js'?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url().'assets/js/sb-admin-2.min.js'?>"></script>

<!-- Custom validate for input pages-->
<!--<script src="--><?php //echo base_url().'assets/vendor/bootstrap/js/bootstrap-validate.js'?><!--"></script>-->
<!--<script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js"></script>-->
<!--<script>-->
<!--    bootstrapValidate('#id','max:10:Please do not enter more than 10 characters')-->
<!--    bootstrapValidate('#name_item','min:10:Enter at least than 10 characters')-->
<!--    bootstrapValidate('#room','alpha:Please fill out this field!')-->
<!--    bootstrapValidate('#building','required:Please fill out this field!')-->
<!--    bootstrapValidate('#id_status','required:Please fill out this field!')-->
<!--    bootstrapValidate('#id_category','required:Please fill out this field!')-->
<!---->
<!--    bootstrapValidate('#who_purchase','required:Please fill out this field!')-->
<!--    bootstrapValidate('#date_purchase','required:Please fill out this field!')-->
<!--    bootstrapValidate('#price','numeric:Please only enter numeric characters!')-->
<!--    bootstrapValidate('#who_received','required:Please fill out this field!')-->
<!--    bootstrapValidate('#date_received','required:Please fill out this field!')-->
<!--    bootstrapValidate('#range_shrinked','numeric:Please only enter numeric characters!')-->
<!---->
<!--</script>-->

</body>

</html>
