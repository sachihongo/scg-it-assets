<!DOCTYPE html>
<html>
<head>
	<title>Data Asset</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
<div class="container">
	<div class="row">
		<h2>Data <small>ASSET IT PT.SCG ReadyMix</small></h2>
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add New</button>
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Nama</th>
				<th>Status</th>
				<th>Room</th>
				<th>QR CODE</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($data as $row):?>
			<tr>
				<td style="vertical-align: middle;"><?php echo $row->name_item;?></td>
				<td style="vertical-align: middle;"><?php echo $row->status_item;?></td>
				<td style="vertical-align: middle;"><?php echo $row->room;?></td>
				<td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row->qr_code;?>"></td>
			</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
    <div class="row">
        <form id="myForm" method="post">
            Nama:
            <input type="text" name="nama" class="required" />
            Alamat:
            <input type="text" name="alamat" class="required" />

            <div class="input-group mb-3">
                <input id="price" type="number" name="price" class="required" placeholder="Price" aria-label="Username" aria-describedby="basic-addon1">
            </div>

            <button type="submit">Submit</button>
        </form>
    </div>
    <div class="row">
        <form id="ajax_form" method="post" action="javascript:void(0)">
<!--            <div class="form-group">-->
<!--                <label for="formGroupExampleInput">Name</label>-->
<!--                <input type="text" name="name" class="form-control" id="formGroupExampleInput" placeholder="Please enter name">-->
<!--            </div>-->
            <!-- Grow In Utility -->
            <div class="col-lg-6">

                <div class="card position-relative">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Item</h6>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" id="id" name="id" class="form-control" placeholder="Id item" aria-label="Id" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" id="name_item" name="name_item" class="form-control" placeholder="Item name" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <select id="id_status" name="id_status" class="custom-select" id="inputGroupSelect01">
                                <option selected>Choose status...</option>
                                <?php foreach ($ss_status as $row): ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->status_item; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <select id="id_category" name="id_category" class="custom-select" id="inputGroupSelect02">
                                <option selected>Choose category...</option>
                                <?php foreach ($dd_category as $row): ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->category_item; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <input id="room" type="text" name="room" class="form-control" placeholder="Room" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <input id="building" type="text" name="building" class="form-control" placeholder="Building" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>

            </div>

            <div class="alert alert-success d-none" id="msg_div">
                <span id="res_message"></span>
            </div>

            <div class="form-group">
                <button type="submit" id="send_form" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</div>

<!-- Modal add new mahasiswa-->
<form action="<?php echo base_url().'index.php/test/save_item'?>" method="post">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add New Mahasiswa</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="id" class="control-label">Kode Barang:</label>
						<input type="text" name="id" class="form-control" id="nim">
					</div>
					<div class="form-group">
						<label for="nama" class="control-label">Nama Item:</label>
						<input type="text" name="name_item" class="form-control" id="nama">
					</div>
                    <div class="form-group">
                        <label for="id_status" class="control-label">Status:</label>
                        <select name="id_status" class="form-control" id="status" required>
                            <option value="">Silahkan Pilih</option>
                            <?php foreach ($ss_status as $row): ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->status_item; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id_category" class="control-label">Kategori:</label>
                        <select name="id_category" class="form-control" id="category" required>
                            <option value="">Silahkan Pilih</option>
                            <?php foreach ($dd_category as $row): ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->category_item; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ruang" class="control-label">Ruang:</label>
                        <input type="text" name="room" class="form-control" id="room">
                    </div>
                    <div class="form-group">
                        <label for="bangunan" class="control-label">Bangunan:</label>
                        <input type="text" name="building" class="form-control" id="building">
                    </div>
                    <label><p>PEMBELIAN</p></label>
                    <div class="form-group">
                        <label for="id" class="control-label">Pembeli :</label>
                        <input type="text" name="who_purchase" class="form-control" id="nim">
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Tanggal Pembelian:</label>
                        <input type="date" name="date_purchase" class="form-control" id="building">
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Harga:</label>
                        <input type="number" name="price" class="form-control" id="building">
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Penerima:</label>
                        <input type="text" name="who_received" class="form-control" id="building">
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Tanggal Pembelian:</label>
                        <input type="date" name="date_received" class="form-control" id="building">
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Masa penyusutan:</label>
                        <input type="number" name="range_shrinked" class="form-control" id="building">
                    </div>

				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>

			</div>
		</div>
	</div>
</form>

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.4.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.validate.js'?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#myForm").validate();
    })
</script type="text/javascript">
<script>
    if ($("#ajax_form").length > 0) {
        $("#ajax_form").validate({

            rules: {
                name_item: {
                    required: true,
                    maxlength: 50
                },

                mobile_number: {
                    required: true,
                    digits:true,
                    minlength: 10,
                    maxlength:12,
                },
                email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                },
            },
            messages: {

                name_item: {
                    required: "Please enter name",
                    maxlength: "Your last name maxlength should be 50 characters long."
                },
                mobile_number: {
                    required: "Please enter contact number",
                    minlength: "The contact number should be 10 digits",
                    digits: "Please enter only numbers",
                    maxlength: "The contact number should be 12 digits",
                },
                email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                },

            },
            submitHandler: function(form) {
                $('#send_form').html('Sending..');
                $.ajax({
                    url: "<?php echo base_url('ajax/store') ?>",
                    type: "POST",
                    data: $('#ajax_form').serialize(),
                    dataType: "json",
                    success: function( response ) {
                        console.log(response);
                        console.log(response.success);
                        $('#send_form').html('Submit');
                        $('#res_message').html(response.msg);
                        $('#res_message').show();
                        $('#msg_div').removeClass('d-none');

                        document.getElementById("ajax_form").reset();
                        setTimeout(function(){
                            $('#res_message').hide();
                            $('#msg_div').hide();
                        },10000);
                    }
                });
            }
        })
    }
</script>
</body>
</html>
