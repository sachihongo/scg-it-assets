<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SCG IT ASSET</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.user.css'?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url().'assets/css/shop-item.css'?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/logo.png'?>"/>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">SCG IT Asset</a>
        <?php if (($this->session->userdata('id_jenis_user'))==3): ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hello <?php echo $this->session->userdata('username')?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url().'user/logout'?>">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
        <?php endif; ?>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">
            <h1 class="my-4">Asset list</h1>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <?php foreach($data as $row):?>
            <div class="card mt-4">
                <img class="card-img-top img-fluid" src="<?php echo base_url().'assets/images/asset_item.png'?>">
                    <div class="card-body">
                        <h3 class="card-title"><?php echo $row->name_item;?></h3>
                        <h4 class="card-title">Status: <?php echo $row->status_item;?></h4>
                        <h4 class="card-title">Kategori: <?php echo $row->category_item;?></h4>
                        <h5><?php echo $row->room;?> - <?php echo $row->building;?></h5>
                        <h5><?php echo $row->location;?></h5>
                        <p class="card-text">Asset bermerek <i>"<?php echo $row->name_item;?>"</i> dibeli pada tanggal <i><?php echo $row->date_purchase;?></i> dengan harga IDR <i><?php echo $row->price;?></i>
                            serta penyusutan selama rentang <i>"<?php echo $row->range_shrinked;?> tahun</i>". Untuk mengakses lebih detail, silahkan tekan tombol dibawah dan lakukan login
                            pada halaman tersebut.
                        </p>
                    </div>
            </div>
            <!-- /.card -->

            <?php if (($this->session->userdata('id_jenis_user'))==3): ?>
            <div class="card card-outline-secondary my-4">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#edit_item<?php echo $row->id;?>">
                    Let's Transfer
                </button>
            </div>
            <?php elseif (($this->session->userdata('id_jenis_user'))!=3):  ?>
            <div class="card card-outline-secondary my-4">
                <a href="<?php echo base_url().'user/login_asset/'.$row->id?>" class="btn btn-success">Let's Transfer asset</a>
            </div>
            <?php endif; ?>

            <?php endforeach;?>

        </div>
        <!-- /.col-lg-9 -->
    </div>

</div>
<!-- /.container -->

<!-- Modal -->
<?php foreach($data as $row):?>
<div class="modal fade" id="edit_item<?php echo $row->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Choose the room & building</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'user/transfer_item/'.$row->id?>">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">From</div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" value="<?php echo $row->room ?>"disabled>
                        </div>
                        <div class="col-md-2">To</div>
                        <div class="col-md-4 ml-auto">
                            <input type="text" name="edit_room" class="form-control">
                        </div>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">From</div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" value="<?php echo $row->building ?>"disabled>
                        </div>
                        <div class="col-md-2">To</div>
                        <div class="col-md-4 ml-auto">
                            <input type="text" name="edit_building" class="form-control">
                        </div>
                    </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">From</div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="<?php echo $row->location ?>"disabled>
                            </div>
                            <div class="col-md-2">To</div>
                            <div class="col-md-4 ml-auto">
                                <input type="text" name="edit_location" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; SCG Indonesia 2019</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url().'assets/vendor/jquery/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.bundle.min.js'?>"></script>

</body>

</html>
