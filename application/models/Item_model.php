<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_model extends CI_Model{

    //Hitung semua untuk dashboard atas

//    function get_for_dash() {
////        $this->db->trans_begin();
////        $this->db->query('SELECT SUM(p.range_shrinked) as d1 FROM purchase p');
////        $this->db->query('SELECT COUNT(i.name_item) as d2 FROM item i');
////        $this->db->trans_complete();
////
////        if ($this->db->trans_status() === FALSE)
////        {
////            $this->db->trans_rollback();
////        }
////        else
////        {
////            $this->db->trans_commit();
////        }
//
//        $queryList = array(
//            "SELECT SUM(p.range_shrinked) as d1 FROM purchase p",
//            "SELECT COUNT(i.name_item) as d2 FROM item i",
//        );
//return $this->db->query($queryList)->result();
////        $sql = "SELECT SUM(p.range_shrinked) as d1, COUNT(i.name_item) as d2, COUNT(s.id) as d3, COUNT(c.category_item) as d4
////            FROM purchase p, item i, status s, category c";
////        return $this->db->query($sql)->result();
//    }

    function get_total_invest(){
        $query = $this->db->query("SELECT SUM(price) AS total FROM purchase");
        $total = (int) $query->row_array()['total'];

        return $total;
    }

    function get_total_item(){
        $totalRows = $this->db->count_all_results('item');
        return $totalRows;
    }

    function get_total_status(){
        $totalRows = $this->db->count_all('status');
        return $totalRows;
    }

    function get_total_category() {
        $totalRows = $this->db->count_all('category');
        return $totalRows;
    }

    function pie_chart(){
        $query =  $this->db->query("SELECT COUNT(i.id) as count, s.status_item 
            FROM item i
            INNER JOIN status s ON i.id_status = s.id
            GROUP BY i.id_status");

        $record = $query->result();
        $d = [];

        foreach($record as $row) {
            $d['label'][] = $row->status_item;
            $d['data'][] = (int) $row->count;
        }
        return json_encode($d);
    }

    function bar_chart(){
        $query2 =  $this->db->query("SELECT SUM(p.price) as sum, c.category_item 
            FROM item i
            INNER JOIN purchase p ON i.id_purchase = p.id
            INNER JOIN category c ON i.id_category = c.id
            GROUP BY i.id_category");

        $record2 = $query2->result();
        $d = [];

        foreach($record2 as $row) {
            $d['label'][] = $row->category_item;
            $d['data'][] = (int) $row->sum;
        }
        return json_encode($d);
    }

    function polar_chart(){
        $query =  $this->db->query("SELECT COUNT(id) as count, room 
            FROM item 
            GROUP BY room");

        $record = $query->result();
        $d = [];

        foreach($record as $row) {
            $d['label'][] = $row->room;
            $d['data'][] = (int) $row->count;
        }
        return json_encode($d);
    }

    //Item_model
    function get_default($id) {
        $this->db->select('*');
        $this->db->from('item', 'purchase','special');
        $this->db->join('purchase', 'purchase.id=item.id_purchase', 'inner');
        $this->db->join('special', 'special.id=item.id_special', 'inner');
        $this->db->join('status', 'status.id=item.id_status', 'inner');
        $this->db->join('category', 'category.id=item.id_category', 'inner');
        $this->db->where('item.id', $id);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
        return false;
    }

    function get_search_item($id)
    {
        $this->db->select('*');
        $this->db->from('item', 'purchase','special');
        $this->db->join('purchase', 'purchase.id=item.id_purchase', 'inner');
        $this->db->join('special', 'special.id=item.id_special', 'inner');
        $this->db->join('status', 'status.id=item.id_status', 'inner');
        $this->db->join('category', 'category.id=item.id_category', 'inner');
        $this->db->where('item.id', $id);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
        return false;
    }

    function get_search_item_2($parsing) {

        $query = $this->db->query("SELECT i.id, i.name_item, s.status_item, c.category_item, i.room, i.building, i.location, p.date_purchase, p.price, p.range_shrinked, i.qr_code
            FROM item i
            INNER JOIN purchase p ON i.id_purchase = p.id
            INNER JOIN special sp ON i.id_special = sp.id
            INNER JOIN status s ON i.id_status = s.id
            INNER JOIN category c ON i.id_category = c.id
            WHERE i.id='$parsing'");
        return $query->result();

//        echo $this->db->query($query)->result();

//        $this->db->select('*');
//        $this->db->from('item');
//        $this->db->join('purchase', 'purchase.id=item.id_purchase', 'inner');
//        $this->db->join('special', 'special.id=item.id_special', 'inner');
//        $this->db->join('status', 'status.id=item.id_status', 'inner');
//        $this->db->join('category', 'category.id=item.id_category', 'inner');
//        $this->db->where('item.id', $parsing);
//        $query = $this->db->get();
//        if($query->num_rows()>0){
//            echo $query->result_array();
//        }
//        return false;
    }

    function get_all_item(){
        $sql = "SELECT i.id, i.name_item, i.id_category, i.room, i.building, i.location, i.brand, i.spec_1, i.spec_2, s.status_item, c.category_item, s.status_item, p.who_purchase, p.date_purchase, p.price, p.who_received, p.date_received, p.range_shrinked, i.qr_code, sp.processor, sp.ram, sp.disk, sp.os, sp.screen, sp.port
            FROM item i
            INNER JOIN purchase p ON i.id_purchase = p.id
            INNER JOIN special sp ON i.id_special = sp.id
            INNER JOIN status s ON i.id_status = s.id
            INNER JOIN category c ON i.id_category = c.id";
        return $this->db->query($sql)->result();
    }

    function count_price(){
//        $this->db->select_sum('status_item');
//        $result = $this->db->get('status');
//        return $result->result();
    }

//    function get_detail_item($id){
//        $this->db->select('*');
//        $this->db->from('item', 'purchase');
//        $this->db->join('purchase', 'purchase.id=item.id_purchase', 'inner');
//        $this->db->join('status', 'status.id=item.id_status', 'inner');
//        $this->db->join('category', 'category.id=item.id_category', 'inner');
//        $this->db->where('item.id', $id);
//        $query = $this->db->get();
//        return $query->row();
//    }

    function add_item($id,$name_item,$room,$building,$location,$id_status,$id_category, $image_name, $id_purchase, $id_item, $id_special, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked, $brand, $spec_1, $spec_2){
        $data_item = array(
            'id'	=> $id,
            'name_item'	=> $name_item,
            'room' => $room,
            'building'	=> $building,
            'location'  => $location,
            'id_purchase' => $id_purchase,
            'id_special' => $id_special,
            'id_status'	=> $id_status,
            'id_category'	=> $id_category,
            'qr_code'   =>  $image_name,
            'brand'     => $brand,
            'spec_1'    => $spec_1,
            'spec_2'    => $spec_2
        );
        $data_purchase = array(
            'id'    => $id_purchase,
            'who_purchase'    => $who_purchase,
            'date_purchase'    => $date_purchase,
            'price'    => $price,
            'who_received'    => $who_received,
            'date_received'    => $date_received,
            'range_shrinked'    => $range_shrinked
        );
        $data_special = array(
            'id' => $id_special
        );

        $this->db->insert('purchase', $data_purchase);
        $this->db->insert('special', $data_special);
        $this->db->insert('item', $data_item);
        $this->db->where('item.id_purchase==purchase.id');
        $this->db->where('item.id_special==special.id');
    }

    function renew_item($id,$name_item,$room,$building,$location,$id_status,$id_category, $id_special, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked, $brand, $spec_1, $spec_2, $processor, $ram, $disk, $os, $screen, $port){
        $data_item = array(
            'name_item'	=> $name_item,
            'room' => $room,
            'building'	=> $building,
            'location'	=> $location,
            'id_status'	=> $id_status,
            'id_category'	=> $id_category,
            'brand'    => $brand,
            'spec_1'    => $spec_1,
            'spec_2'    => $spec_2
        );
        $data_purchase = array(
            'who_purchase'    => $who_purchase,
            'date_purchase'    => $date_purchase,
            'price'    => $price,
            'who_received'    => $who_received,
            'date_received'    => $date_received,
            'range_shrinked'    => $range_shrinked
        );
        $data_special = array(
            'processor' => $processor,
            'ram'   => $ram,
            'disk'  => $disk,
            'os'    => $os,
            'screen'    => $screen,
            'port'      => $port
        );

        $this->db->where('purchase.id', $id);
        $this->db->update('purchase', $data_purchase);

        $this->db->where('special.id', $id_special);
        $this->db->update('special', $data_special);

        $this->db->where('item.id_purchase', $id);
        $this->db->update('item', $data_item);
    }

    function relocate_item($id, $room, $building, $location){
        $data_item = array(
            'room'  => $room,
            'building' => $building,
            'location'  => $location
        );
        $this->db->where('item.id', $id);
        $this->db->update('item', $data_item);
    }

    function remove_item($id) {

        $sql = "DELETE item,purchase,special 
        FROM item,purchase,special 
        WHERE item.id_purchase=purchase.id 
        AND item.id_special=special.id 
        AND item.id= ?";

        $pathimage = './assets/images/qrcode/'.$id.'.png';
        unlink($pathimage);

        $this->db->query($sql, array($id));
    }

    //Status_Item model

    function add_status($status_item, $description_status) {
        $data = array(
            'status_item'           => $status_item,
            'description_status'    => $description_status
        );
        $this->db->insert('status', $data);
    }

    function get_status() {
        $this->db->select('*');
        $this->db->from('status');
        $result = $this->db->get();
        return $result->result();
    }

    function count_status(){
        $sql = "SELECT status_item, COUNT(*) 
            FROM status GROUP BY status_item";
        return $this->db->query($sql)->result();
    }

    function renew_status($id, $status_item, $description_status) {
        $data_status = array(
            'status_item'  => $status_item,
            'description_status' => $description_status
        );
        $this->db->where('status.id', $id);
        $this->db->update('status', $data_status);
    }

    function remove_status($id) {
        $this->db->where('id', $id);
        $this->db->delete('status');
    }

    //Category_Item model
    function add_category($category_item, $description_category) {
        $data = array(
            'category_item'           => $category_item,
            'description_category'    => $description_category
        );
        $this->db->insert('category', $data);
    }

    function get_category() {
        $this->db->select('*');
        $this->db->from('category');
        $result = $this->db->get();
        return $result->result();
    }

    function count_category(){
        return $this->db->count_all_results('category');
//        return $result->result();
    }

    function renew_category($id, $category_item, $description_category) {
        $data_category = array(
            'category_item'  => $category_item,
            'description_category' => $description_category
        );
        $this->db->where('category.id', $id);
        $this->db->update('category', $data_category);
    }

    function remove_category($id) {
        $this->db->where('id', $id);
        $this->db->delete('category');
    }

    function get_room(){
//        $sql =  $this->db->query("SELECT room
//            FROM item
//            GROUP BY room");
//        return $this->db->query($sql)->result();


        $this->db->select('room');
        $this->db->group_by('room');
        $result = $this->db->get('item');
        return $result->result();
    }

}