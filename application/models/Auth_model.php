<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model{

    private $_table = "user";

    function cek($username, $password){
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('user');
    }

    function pie_chart2(){
        $query =  $this->db->query("SELECT COUNT(id_user) as count, id_jenis_user 
            FROM user
            GROUP BY id_jenis_user");

        $record = $query->result();

        foreach($record as $row) {
            $d['label'][] = $row->id_jenis_user;
            $d['data'][] = (int) $row->count;
        }
        return json_encode($d);
    }

    function add_user($username, $password, $id_jenis_user, $image) {
        $data = array(
            'username'              => $username,
            'password'              => $password,
            'id_jenis_user'         => $id_jenis_user,
            'image'                 => $image
        );
        $this->db->insert('user', $data);
    }

    function get_user() {
        $this->db->select('*');
        $this->db->from('user');
        $result = $this->db->get();
        return $result->result();
    }

    function get_image($user_session) {
        $this->db->select('image');
        $this->db->from('user');
        $result = $this->db->get();
        return $result->result();
    }

    function renew_user($id, $username, $password, $id_jenis_user, $image) {
        $data_user = array(
            'username'  => $username,
            'password' => $password,
            'id_jenis_user'  => $id_jenis_user,
            'image' => $image
        );
        $this->db->where('user.id_user', $id);
        $this->db->update('user', $data_user);
    }

    function remove_user($id) {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id_user" => $id));
    }

    private function _deleteImage($id)
    {
        $this->db->where('id_user',$id);
        $user = $this->db->get('user')->result()[0];
        $filename = explode(".", $user->image)[0];

        return array_map('unlink', glob(FCPATH."assets/images/user/$filename.*"));
    }

}