<?php
class Asset_model extends CI_Model {
	function get_all_asset(){
		$hasil=$this->db->get('asset');
		return $hasil;
	}

	function simpan_asset($id, $nama, $kategori, $image_name){
		$data = array(
			'id'	=> $id,
			'nama'	=> $nama,
			'kategori' => $kategori,
			'qr_code'	=> $image_name
		);
		$this->db->insert('asset', $data);
	}
}
