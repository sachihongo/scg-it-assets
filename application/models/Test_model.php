<?php
class Test_model extends CI_Model {
	function get_all_asset(){
		$hasil=$this->db->get('item');
		return $hasil;
	}

    function get_status(){
	    $this->db->select('*');
	    $this->db->from('status');
	    $result = $this->db->get();
	    return $result->result();
    }

    function get_category(){
        $this->db->select('*');
        $this->db->from('category');
        $result = $this->db->get();
        return $result->result();
    }

    function get_all_item(){
        $sql = "SELECT i.id, i.name_item, i.room, i.building, s.status_item, c.category_item, s.status_item, p.who_purchase, p.date_purchase, p.price, p.who_received, p.date_received, p.range_shrinked, i.qr_code
            FROM item i
            INNER JOIN purchase p ON i.id_purchase = p.id
            INNER JOIN status s ON i.id_status = s.id
            INNER JOIN category c ON i.id_category = c.id";
        return $this->db->query($sql)->result();
    }

    function add_item($id,$name_item,$room,$building,$id_status,$id_category, $image_name, $id_purchase, $id_item, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked){
        $data_item = array(
            'id'	=> $id,
            'name_item'	=> $name_item,
            'room' => $room,
            'building'	=> $building,
            'id_purchase' => $id_purchase,
            'id_status'	=> $id_status,
            'id_category'	=> $id_category,
            'qr_code'   =>  $image_name
        );
        $data_purchase = array(
            'id'    => $id_purchase,
            'who_purchase'    => $who_purchase,
            'date_purchase'    => $date_purchase,
            'price'    => $price,
            'who_received'    => $who_received,
            'date_received'    => $date_received,
            'range_shrinked'    => $range_shrinked
        );

        $this->db->insert('purchase', $data_purchase);
        $this->db->insert('item', $data_item);
        $this->db->where('item.id_purchase==purchase.id');
    }

	function simpan_item($id,$name_item,$room,$building,$id_status,$id_category, $id_purchase, $id_item, $who_purchase, $date_purchase, $price, $who_received, $date_received, $range_shrinked){
		$data = array(
			'id'	=> $id,
			'name_item'	=> $name_item,
			'room' => $room,
			'building'	=> $building,
			'id_purchase' => $id_purchase,
			'id_status'	=> $id_status,
			'id_category'	=> $id_category,
		);
		$data2 = array(
		    'id'    => $id_purchase,
            'id_item'    => $id_item,
            'who_purchase'    => $who_purchase,
            'date_purchase'    => $date_purchase,
            'price'    => $price,
            'who_received'    => $who_received,
            'date_received'    => $date_received,
            'range_shrinked'    => $range_shrinked
        );
		$this->db->insert('item', $data);
		$this->db->insert('purchase', $data2);
	}
}
